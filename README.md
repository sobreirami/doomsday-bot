# ChatBot do Apocalipse

Bot para o Telegram, escrito ​em PHP​,que recebe do usuário a seguinte pergunta: 

“Algum asteroide oferece perigo ao planeta Terra na data de hoje?”

## Instalação
Após o clone ou download desse projeto use o gerenciador de pacotes [composer](https://getcomposer.org/) para instalar.

```
php composer.phar install
```

## Configuração
No arquivo .env configure os seguintes parâmetros

```
TELEGRAM_API_KEY=

TELEGRAM_BOT_USERNAME=ChatDoApocalipseBot

TELEGRAM_URL_WEBHOOK=https://chatbot-do-apocalipse.herokuapp.com/webhook
```
## Rodando localmente
Após a instalação e configuração do projeto.

```
php -S localhost:8080 -t public public/index.php
```

## Rotas

GET "/" para registra o webhook

POST "/webhook" para enviar os comandos ao telegram
```
/start = Inicia o chat
Algum asteroide oferece perigo ao planeta Terra na data de hoje? = responde a pergunta com base na api da nasa
```

DELETE /delete para remover o webhook

## Justificativas
Como é um projeto de pequeno porte escolhi pelo micro framework silex ele é rápido e extensível. 

Não achei interessante usar um pacote como "php-telegram-bot" porque o objetivo do projeto é muito claro e simples o telegram também possui api http muito simples e fácil de implementar reforçando a minha idéia.

Criei o projeto baseado no skeleton do slim 4 e publiquei no heroku como orientado para utilizar o bot procure pelo username "ChatDoApocalipseBot"