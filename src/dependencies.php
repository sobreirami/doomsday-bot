<?php

use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        'renderer' => function (ContainerInterface $c) { 
            $settings = $c->get('settings')['renderer'];
            return new Slim\Views\PhpRenderer($settings['template_path']);
        },
        'mailer' => function (ContainerInterface $c) {
            $settings = $c->get('settings')['mailer'];

            $transport = (new Swift_SmtpTransport($settings['host'], $settings['port']))
            ->setUsername($settings['username'])
            ->setPassword($settings['password']);

            return new Swift_Mailer($transport);
        },
        'view' => function (ContainerInterface $c) {
            $view = new \Slim\Views\Twig(__DIR__ . '/../templates');
            $basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');
            $view->addExtension(new \Slim\Views\TwigExtension($c['router'], $basePath));
            
            return $view;
        },
        'telegram' => function (ContainerInterface $c) {
            $settings = $c->get('settings')['telegram'];
            $ef = new app\Telegram($settings);
            return $ef;
        }
    ]);
};