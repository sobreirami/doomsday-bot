<?php

use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'displayErrorDetails' => true, // Should be set to false in production
            'renderer' => [
                'template_path' => __DIR__ . '/../templates/',
            ],
            'mailer' => [
                'host' => getenv('MAIL_HOST') ?? "",
                'port' => getenv('MAIL_PORT') ?? "",
                'username' => getenv('MAIL_USERNAME') ?? "",
                'password' => getenv('MAIL_PASSWORD') ?? "",
                'encryption' => getenv('MAIL_ENCRYPTION') ?? ""
            ],
            'telegram' => [
                'api' => getenv('TELEGRAM_API_KEY') ?? "",
                'bot_username' => getenv('TELEGRAM_BOT_USERNAME') ?? "",
                'url_webhook' => getenv('TELEGRAM_URL_WEBHOOK') ?? ""
            ]
        ],
    ]);
};
