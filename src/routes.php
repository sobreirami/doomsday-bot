<?php

use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

function searchNasa($startDate, $endDate) {

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.nasa.gov/neo/rest/v1/feed?start_date={$startDate}&end_date={$endDate}&api_key=cy5PkiAqrSMPsjmejGXVBbBQr2zDNdDbJw62acon",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET"
    ));

    $data = curl_exec($curl);
    curl_close($curl);

    return json_decode($data, true);
}

return function (App $app) {

    $app->options('/{routes:.+}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function(Request $request, Response $response) {
        $telegram = $this->get('telegram'); 
        $out = $telegram->register();

        $payload = json_encode($out);
    
        $response->getBody()->write($payload);
        return $response
            ->withHeader('Content-Type', 'application/json');
    });
    
    $app->post('/webhook', function (Request $request, Response $response) {
           
        $dataBody = $request->getBody()->getContents();
        $messageData = json_decode($dataBody, true);
        
        $out = [];

        if (isset($messageData["message"])) {
            
            $message = $messageData["message"];

            $telegram = $this->get('telegram'); 
            $telegram->setChatId($message['chat']['id']);

            if (isset($message['text'])) {
    
                $messageText = $message['text'];
    
                if (strpos($messageText, "/start") === 0) {                  
    
                    $text = "Eu posso ajudá-lo a saber se algum asteroide oferece perigo ao planeta Terra para isso é só perguntar. Ex: Algum asteroide oferece perigo ao planeta Terra na data de hoje?";                    
                    $out = $telegram->send($text);               
                }
    
                $messageText = substr(trim(strtolower($messageText)), 0, -1);
                
                if (strpos($messageText, "algum asteroide oferece perigo ao planeta terra na data de hoje") === 0) {
    
                    // SEARCH NASA
    
                    $now = date('Y-m-d');
    
                    $objects = searchNasa($now, $now);
                    
                    $text = "Não";
    
                    if($objects || count($objects) > 0) {
                        
                        $danger = false;
                        $dataAsteroids = "";
    
                        if($objects['element_count'] > 0) {
    
                            foreach($objects['near_earth_objects'] as $objectsDate) {
    
                                foreach($objectsDate as $object) {
                                    if($object['is_potentially_hazardous_asteroid'] == true) {
                                        $danger = true;
                                        
                                        $DiameterMin      = $object['estimated_diameter']['kilometers']['estimated_diameter_min'];
                                        $DiameterMax      = $object['estimated_diameter']['kilometers']['estimated_diameter_max'];
                                        $missDistance     = $object['close_approach_data'][0]['miss_distance']['kilometers']; 
                                        $relativeVelocity = $object['close_approach_data'][0]['relative_velocity']['kilometers_per_hour']; 
    
                                        $dataAsteroids .= "\n\nNome: " . $object['name'] 
                                                . "\nDiâmetro estimado (em Km): de " . $DiameterMin . " à " .  $DiameterMax
                                                . "\nDistância em que ele irá passar em relação à Terra (em Km): " .  $missDistance
                                                . "\nVelocidade relativa à Terra  (em Km/h): " .  $relativeVelocity
                                                . "\nMais detalhes: " .  $object['nasa_jpl_url'];
                                    }
        
                                }
                               
                            }
                        }
    
                        if($danger == true) {
                            $text = "Sim, segue os asteroides potenciamente perigosos: " . $dataAsteroids;
                        }
    
                    }
    
                    $out = $telegram->send($text);    
    
                }
                
            }
    
        }
    
        $payload = json_encode($out);
    
        $response->getBody()->write($payload);
        return $response
            ->withHeader('Content-Type', 'application/json');
    
    });

    $app->delete('/delete', function (Request $request, Response $response) {
        $telegram = $this->get('telegram'); 
        $out = $telegram->delete();

        $payload = json_encode($out);
    
        $response->getBody()->write($payload);
        return $response
            ->withHeader('Content-Type', 'application/json');
    });
};