<?php

namespace app;

class Telegram {
    
    private $telegram;

    protected $chatId;

    public function __construct($telegram) {
        $this->telegram = $telegram;
    }
    
    public function send($message) {
        
        $curl = curl_init();
        $api = $this->telegram['api'];

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.telegram.org/bot{$api}/sendMessage",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n\t\"text\": \"{$message}\",\n\t\"chat_id\": {$this->chatId}\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));

        $ret = curl_exec($curl);
        curl_close($curl);

        return json_decode($ret, true);
    }

    public function register() {
        $curl = curl_init();
        $api = $this->telegram['api'];
        $urlWebhook = $this->telegram['url_webhook'];

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.telegram.org/bot{$api}/setwebhook?url={$urlWebhook}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET"
        ));

        $ret = curl_exec($curl);
        curl_close($curl);

        return json_decode($ret, true);
    }

    public function delete() {
        $curl = curl_init();
        $api = $this->telegram['api'];

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.telegram.org/bot{$api}/deleteWebhook",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET"
        ));

        $ret = curl_exec($curl);
        curl_close($curl);

        return json_decode($ret, true);
    }

    public function setChatId($chatId)  {
        $this->chatId = $chatId;
    }

}